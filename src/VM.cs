﻿// venkata narasimha reddy sanivarapu
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace JoesPets
{

    class VM : INotifyPropertyChanged
    {
        decimal total;
        #region constants
        const decimal CricketsPerKg = 24.75m;
        const decimal WormsPerKg = 44.99m;
        const decimal MiceEach = 19.99m;
        const decimal SalesTax = 15;
        const decimal DeliveryCharge = 15;
        #endregion

        private string totalLabel = "";
        public string TotalLabel
        {
            //get accessor is used to return a property value and a set accessor is used to assign a new value.
            get { return totalLabel; }
            set { totalLabel = value; NotifyChange(); }
        }
        private string crickets = "";
        public string Crickets
        {
            //get accessor is used to return a property value and a set accessor is used to assign a new value.
            get { return crickets; }
            set { crickets = value; NotifyChange(); }
        }
        private string worms = "";
        public string Worms
        {
            //get accessor is used to return a property value and a set accessor is used to assign a new value.
            get { return worms; }
            set { worms = value; NotifyChange(); }
        }
        private string mice = "";
        public string Mice
        {
            //get accessor is used to return a property value and a set accessor is used to assign a new value.
            get { return mice; }
            set { mice = value; NotifyChange(); }
        }
        private bool deliveryCheck = false;
        public bool DeliveryCheck
        {
            //get accessor is used to return a property value and a set accessor is used to assign a new value.
            get { return deliveryCheck; }
            set { deliveryCheck = value; NotifyChange(); }
        }

        public void Calculate()
        {
            if (string.IsNullOrEmpty(crickets))
            {
                crickets = "0";
            }
            try
            {
                total = (decimal.Parse(crickets) * CricketsPerKg) + (decimal.Parse(worms) * WormsPerKg) + (decimal.Parse(mice) * MiceEach);
            }
            catch
            {
                MessageBox.Show("Enter a valid input");
            }
            TotalLabel = total.ToString();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyChange([CallerMemberName] string property = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
